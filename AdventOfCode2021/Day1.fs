namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay1 =

    let part1 () =
        let inputs =
            Seq.map int (Utils.readLinesSeq "input1.txt")

        ((0, 0), inputs)
        ||> Seq.fold
                (fun (acc, prevVal) input ->
                    let acc' = if input > prevVal then acc + 1 else acc
                    (acc', input))
        |> fst


    let part2 () =
        Utils.readLinesSeq "input1.txt"
        |> Seq.map int
        |> Seq.windowed 3
        |> Seq.map
            (fun listTriple ->
                match listTriple with
                | [| a; b; c |] -> a + b + c
                | _ -> failwith Utils.errMsg)
        |> Seq.pairwise
        |> Seq.filter (fun (a, b) -> a < b)
        |> Seq.length
