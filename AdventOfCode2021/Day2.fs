namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay2 =
    let part1 () =
        Utils.readLinesSeq "input2.txt"
        |> Seq.map
            (fun (s: string) ->
                match Seq.toList (s.Split " ") with
                | [ cmd; value ] -> (cmd, (int) value)
                | _ -> failwith Utils.errMsg)
        |> fun s -> ((0, 0), s)
        ||> Seq.fold
                (fun (hor, dep) (cmd, value) ->
                    match cmd with
                    | "forward" -> (hor + value, dep)
                    | "down" -> (hor, dep + value)
                    | "up" -> (hor, dep - value)
                    | _ -> failwith Utils.errMsg)
        |> fun (horizontal, depth) -> horizontal * depth

    // 1990000


    let part2 () =
        Utils.readLinesSeq "input2.txt"
        |> Seq.map
            (fun (s: string) ->
                match Seq.toList (s.Split " ") with
                | [ cmd; value ] -> (cmd, (int) value)
                | _ -> failwith Utils.errMsg)
        |> ((0, 0, 0)
            |> Seq.fold
                (fun (hor, dep, aim) (cmd, value) ->
                    match cmd with
                    | "forward" -> (hor + value, value * aim + dep, aim)
                    | "down" -> (hor, dep, aim + value)
                    | "up" -> (hor, dep, aim - value)
                    | _ -> failwith Utils.errMsg))
        |> fun (horizontal, depth, _) -> horizontal * depth
