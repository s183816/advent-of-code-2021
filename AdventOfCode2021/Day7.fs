namespace AdventOfCode2021

open AdventOfCode2021.Internals



module AOCDay7 =

    let input: int list =
        Utils.readLines "input7.txt"
        |> List.head // Input is only one line
        |> fun (s: string) -> Seq.toList (s.Split ",") // Split at commas
        |> List.map int

    let test = [ 16; 1; 2; 0; 4; 2; 7; 1; 2; 14 ]

    let part1 () =

        // The data used for the result
        let data = input

        // Function: Get the sum of the differences between a value y and all
        // values in a list xs.
        let sumOfDifferences xs y =
            (0, xs)
            ||> List.fold (fun sum x -> sum + abs (x - y))

        let range = [ List.min data .. List.max data ]
        let fuelCosts = List.map (sumOfDifferences data) range
        List.find (fun fc -> fc = List.min fuelCosts) fuelCosts

    let part2 () =

        let data = input

        let gauss nat = nat * (nat + 1) / 2


        let distance a b = gauss (abs (a - b))

        let sumOfDistances xs y =
            (0, xs)
            ||> List.fold (fun sum x -> sum + distance x y)

        let range = [ List.min data .. List.max data ]
        let fuelCosts = List.map (sumOfDistances data) range
        List.find (fun fc -> fc = List.min fuelCosts) fuelCosts
