// Inspired by 'AdventOfCode2021/Utils.fs' from this GitHub repo:
// https://github.com/Smaug123/AdventOfCode2021
namespace AdventOfCode2021.Internals

open System.IO

module Utils =

    // Code snippet from here: https://stackoverflow.com/a/2365548
    let readLinesSeq (fileName: string) =
        seq {
            let filePath =
                __SOURCE_DIRECTORY__ + "/Inputs/" + fileName

            use sr = new StreamReader(filePath)

            while not sr.EndOfStream do
                yield sr.ReadLine()
        }

    let readLines (filePath: string) = Seq.toList (readLinesSeq filePath)

    let printList array = printfn $"%A{array}"
    let printInt (n: int) = printfn $"%i{n}"

    let errMsg = "Houston, we have a problem."
