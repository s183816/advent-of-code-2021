namespace AdventOfCode2021

open System
open AdventOfCode2021.Internals

module AOCDay12 =

    type CaveSystem = Map<Cave, Connections>
    and Cave = string 
    and Connections = Cave Set

    // Caves are big if all the letters are uppercase
    let isSmallCave (cave : Cave) = Seq.forall Char.IsLower cave
    let isBigCave (cave : Cave) = not (isSmallCave cave)

    // Parse the input to a cave system
    let connect c1 c2 caveSystem : CaveSystem = 
        match Map.tryFind c1 caveSystem  with 
        | None -> Map.add c1 (Set [c2]) caveSystem
        | Some cs -> Map.add c1 (Set.add c2 cs) caveSystem
    let addConnection c1 c2 caveSystem = 
        connect c1 c2 caveSystem |> connect c2 c1
    let connectionFromInputLine caveSystem (inputLine : String) =
        match Seq.toList (inputLine.Split "-") with 
        | [cave1; cave2] -> addConnection cave1 cave2 caveSystem
        | _ -> failwith Utils.errMsg
    let createCaveSystem input : CaveSystem = 
        let initCS = Map [("start", Set.empty); ("end", Set.empty)] 
        (initCS, input) ||> List.fold connectionFromInputLine

    // Find all possible distinct paths using depth-first-search (dfs) 
    let rec sumOfPathsAux vs cave cs = 
        if cave = "end" then 1 
        else 
            let vs' = if isSmallCave cave then Set.add cave vs else vs 
            let mutable sum = 0 
            for c in Map.find cave cs do 
                if not (Set.contains c vs') then 
                    sum <- sum + sumOfPathsAux vs' c cs
            sum
    let sumOfPaths cs = sumOfPathsAux Set.empty "start" cs

    // ! PART 1
    let part1 () = 
        Utils.readLines "input12.txt" 
        |> createCaveSystem
        |> sumOfPaths

    // A bit more complex way of defining paths than in part 1
    // The main idea: Add all the paths to the global list paths, that is a 
    // list of lists of caves, ie. a list of paths. When all paths have been 
    // found, some might appear more than once. To fix this, List.distinct is 
    // used to rule out any duplicates. 
    let mutable paths = []
    let rec sumOfPathsAux2 (vs,x) cave cs = 
        if cave = "end" then paths <- List.rev (cave::vs) :: paths
        else if isBigCave cave then proceed (cave::vs,x) cave cs 
        else handleSmallCave (vs,x) cave cs 
    and handleSmallCave (vs,x) cave cs = 
        let vs' = cave::vs 
        if x = "" && cave <> "start" then 
            if List.contains cave vs then proceed (vs',cave) cave cs 
            else 
                proceed (vs',cave) cave cs 
                proceed (vs',x) cave cs 
        else if x = cave then 
            if List.length (List.filter (fun v -> v = cave) vs) < 2 then 
                proceed (vs',x) cave cs 
        else if not (List.contains cave vs) then proceed (vs',x) cave cs 
    and proceed (vs,x) cave cs = 
        for c in Map.find cave cs do 
            sumOfPathsAux2 (vs,x) c cs 
    and sumOfUniquePaths2 cs = 
        sumOfPathsAux2 ([],"") "start" cs 
        List.distinct paths |> List.length
        


    let part2 () = 
        Utils.readLines "input12.txt" 
        |> createCaveSystem
        |> sumOfUniquePaths2
