namespace AdventOfCode2021

open AdventOfCode2021.Internals

open System.IO

module AOCDay4 =
    let input = Utils.readLines "input4.txt"

    let numbers =
        List.head input
        |> fun s -> Seq.toList (s.Split ",")
        |> List.map int

    let boards =
        List.tail input
        |> List.filter (fun e -> e <> "")
        |> List.map (fun s -> Seq.toList (s.Split " "))
        |> List.map (List.filter (fun e -> e <> ""))
        |> List.map (List.map int)
        |> fun bs -> List.splitInto (List.length bs / 5) bs

    let crossOut n board =
        board
        |> List.map (List.map (fun e -> if e = n then -1 else e))

    let checkBoard board =
        let b1 =
            List.transpose board
            |> List.exists (fun row -> List.sum row = -5)

        let b2 =
            board
            |> List.exists (fun row -> List.sum row = -5)

        b1 || b2

    let getResult (board, number) =
        (0, board)
        ||> List.fold (fun acc xs -> acc + List.sum (List.filter (fun x -> x >= 0) xs))
        |> fun sum -> sum * number

    let part1 () =
        let rec getWinner boards =
            function
            | [] -> failwith "No winner"
            | n :: nrest ->
                let boards' = List.map (crossOut n) boards

                match List.tryFind checkBoard boards' with
                | None -> getWinner boards' nrest
                | Some b -> (b, n)

        getWinner boards numbers |> getResult


    let part2 () =
        let rec getLoser boards =
            function
            | [] -> failwith "No single loser"
            | n :: nrest ->
                let boards' = List.map (crossOut n) boards

                if List.forall checkBoard boards' then
                    List.findIndex (fun b -> not (checkBoard b)) boards
                    |> fun i -> (i, boards')
                    ||> List.item
                    |> fun b -> (b, n)
                else
                    getLoser boards' nrest

        getLoser boards numbers |> getResult
