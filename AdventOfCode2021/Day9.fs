namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay9 = 

    let charToInt c = int c - int '0'
    let parsedInput = 
        Utils.readLines "input9.txt"
        |> List.map (Seq.toList >> List.map charToInt) 
    
    let maxX = List.length parsedInput - 1
    let maxY = List.length parsedInput.[0] - 1    

    let isLowPoint x y (grid : int list list) = 
            let n = grid.[x].[y]
            (x = 0 || grid.[x-1].[y] > n)  
                && (y = 0 || grid.[x].[y-1] > n) 
                && (x = maxX || grid.[x+1].[y] > n)
                && (y = maxY || grid.[x].[y+1] > n)

    let part1 () = 
        let mutable sum = 0
        for x in [0 .. maxX] do
            for y in [0 .. maxY] do 
                if isLowPoint x y parsedInput then 
                    let n = parsedInput.[x].[y]
                    sum <- sum + n + 1
        sum


    let mutable visited = Set.empty

    let condPush b e list = if b then e::list else list 
    let getBasinNeighbors x y (grid : int list list) = 
        List.empty 
        |> condPush (x > 0) (x-1,y)
        |> condPush (y > 0) (x,y-1) 
        |> condPush (x < maxX) (x+1,y) 
        |> condPush (y < maxY) (x,y+1)
        |> List.filter (fun (x',y') -> 
            grid.[x'].[y'] <> 9 && not (Set.contains (x',y') visited))
    
    let rec dfs x y (grid : int list list) = 
        if grid.[x].[y] = 9 || Set.contains (x,y) visited then 0 
        else 
            visited <- Set.add (x,y) visited 
            let mutable sum = 0 
            for (x',y') in getBasinNeighbors x y grid do 
                sum <- sum + dfs x' y' grid
            1 + sum

    let part2 () = 

        let mutable basinSizes = List.empty 
        for x in [0 .. maxX] do
            for y in [0 .. maxY] do 
                if not (Set.contains (x,y) visited) then 
                    basinSizes <- dfs x y parsedInput::basinSizes
        
        let topThree = 
            basinSizes 
            |> List.sortDescending
            |> List.take 3 
        
        (1,topThree) ||> List.fold (*)



        