namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay11 =

    let sample = "sample11.txt"
    let input = "input11.txt"

    // The data used when running the program
    let data = input

    type Octopus = EnergyLevel * Flashed
    and EnergyLevel = int 
    and Flashed = bool 

    // Generate (global) 2D array of octopi/octopuses from the puzzle input
    let charToInt c = int c - int '0'
    let nrgLvlToOct nrgLvl : Octopus = (nrgLvl,false)
    let mutable OCTOPI : Octopus [,] = 
        Utils.readLines data
        |> List.map (Seq.toList >> List.map (charToInt >> nrgLvlToOct))
        |> array2D

    let ROWS, COLS = Array2D.length1 OCTOPI, Array2D.length2 OCTOPI

    // Increment energy for one octopus or the global octopi array
    let incrementEnergy (e,f) : Octopus = (e+1,f)
    let incrementAll () = OCTOPI <- Array2D.map incrementEnergy OCTOPI
    
    // Flash an octopus and increment neighbouring octopi energy levels. 
    let rec flash row col = 
        let (e,f) = OCTOPI[row, col] 
        if e > 9 && not f then 
            OCTOPI[row, col] <- (e,true)
            incNeighbours row col
    and incNeighbours row col = 
        for r in [max 0 (row-1) .. min (ROWS-1) (row+1)] do 
            for c in [max 0 (col-1) .. min (COLS-1) (col+1)] do 
                let (e,f) = incrementEnergy OCTOPI[r,c]
                OCTOPI[r,c] <- (e,f)
                if e > 9 then flash r c 

    // Initiate flash for all eligable octopi 
    let flashOctopi () = OCTOPI |> Array2D.iteri (fun r c _ -> flash r c) 

    // Count the flashed octopi. Reset them to energy level 0. 
    let countAndResetFlashed () = 
        let mutable sum = 0
        OCTOPI <- OCTOPI |> Array2D.map (fun (e,f) -> 
            if f then 
                sum <- sum + 1
                (0,false) 
            else (e,false)) 
        sum

    // Perform the steps in part 1 (Note, call performSteps function)
    let rec performStepsAux flashes = function 
        | 0 -> flashes 
        | n -> 
            incrementAll () 
            flashOctopi () 
            let fs' = countAndResetFlashed () + flashes
            performStepsAux fs' (n-1)
    and performSteps steps = performStepsAux 0 steps

    // ! PART 1
    let part1 () = performSteps 100

    // Detect if all octopi have flashed. 
    let allFlashed () = 
        let mutable forAllFlashed = true 
        OCTOPI |> Array2D.iter (fun (e,_) -> 
            forAllFlashed <- forAllFlashed && e = 0)
        forAllFlashed

    // First step where all octopi flash
    let rec stepForAllFlashed step =
        if allFlashed () then step 
        else 
            incrementAll () 
            flashOctopi () 
            let _ = countAndResetFlashed ()
            stepForAllFlashed (step+1) 

    // ! PART 2
    let part2 () = stepForAllFlashed 0