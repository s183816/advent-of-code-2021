namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay5 =

    type Point = int * int
    type Line = Point * Point

    let input = Utils.readLines "input5.txt"

    let rec getLineSegments acc (input: string list) : Line list =
        match input with
        | [] -> List.rev acc
        | inputLine :: inputRest ->
            let (p1, p2) =
                inputLine.Split " -> "
                |> Seq.toList
                |> List.map (
                    fun (s: string) -> Seq.toList (s.Split ",")
                    >> List.map int
                    >> fun listTuple ->
                        match listTuple with
                        | [ a; b ] -> (a, b)
                        | _ -> failwith Utils.errMsg
                )
                |> fun inputTuple ->
                    match inputTuple with
                    | [ p1'; p2' ] -> (p1', p2')
                    | _ -> failwith Utils.errMsg

            getLineSegments ((p1, p2) :: acc) inputRest

    let getDiagonalValues (p1: Point) (p2: Point) =
        let xs =
            [ min (fst p1) (fst p2) .. max (fst p1) (fst p2) ]

        let ys =
            [ min (snd p1) (snd p2) .. max (snd p1) (snd p2) ]

        let xs' =
            if List.head xs = fst p2 then
                List.rev xs
            else
                xs

        let ys' =
            if List.head ys = snd p2 then
                List.rev ys
            else
                ys

        List.zip xs' ys'

    let getAllPoints includeDiag lineSegments =
        lineSegments
        |> List.map
            (fun (p1, p2) ->
                if fst p1 = fst p2 then
                    [ min (snd p1) (snd p2) .. max (snd p1) (snd p2) ]
                    |> List.map (fun x -> (fst p1, x))
                else if snd p1 = snd p2 then
                    [ min (fst p1) (fst p2) .. max (fst p1) (fst p2) ]
                    |> List.map (fun x -> (x, snd p2))
                else if includeDiag then
                    getDiagonalValues p1 p2
                else
                    failwith "Invalid points")
        |> ([] |> List.fold (fun acc line -> line @ acc))


    let getLineCrossings points =
        (Map.empty, points)
        ||> List.fold
                (fun acc p ->
                    match Map.tryFind p acc with
                    | None -> Map.add p 1 acc
                    | Some n -> Map.add p (n + 1) acc)
        |> Map.filter (fun _ n -> n > 1)
        |> Map.count


    let part1 () =
        getLineSegments [] input
        |> List.filter (fun (p1, p2) -> fst p1 = fst p2 || snd p1 = snd p2)
        |> getAllPoints false
        |> getLineCrossings


    let part2 () =
        getLineSegments [] input
        |> getAllPoints true
        |> getLineCrossings
