namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay10 =

    let sample = "sample10.txt"
    let input = "input10.txt"

    let pairs = Map [('(', ')'); ('[', ']'); ('{', '}'); ('<', '>')]

    let rec traverse closers = function 
        | [] -> ' ' 
        | c::cs -> 
            if Map.containsKey c pairs then 
                traverse (Map.find c pairs :: closers) cs 
            else 
                if List.head closers = c then traverse (List.tail closers) cs 
                else c
    
    let errScores = Map [(' ', 0); (')', 3); (']', 57); ('}', 1197); ('>', 25137)]

    let part1 () = 
        Utils.readLines input 
        |> List.map (Seq.toList >> traverse []) 
        |> List.fold (fun sum c -> sum + Map.find c errScores) 0

    
    // Note: The corrupted lines are discarded. Not very effeciently though, as 
    // the current implementation runs through the arrays twice. 
    let rec findMissingAux ops = function
        | [] -> ops 
        | c::crest -> 
            if Map.containsKey c pairs then findMissingAux (c :: ops) crest
            else findMissingAux (List.tail ops) crest
    let findMissing cs = 
        if traverse [] cs = ' ' then findMissingAux [] cs else []
        
    let replaceWithCloser opener = Map.find opener pairs

    let errScores2 = Map [(')', 1UL); (']', 2UL); ('}', 3UL); ('>', 4UL)]
    let toScore c = Map.find c errScores2

    let rec getLinePenalty acc = function 
        | [] -> acc 
        | n::nrest -> getLinePenalty (acc * 5UL + n) nrest

    let median list = List.sort list |> List.item (List.length list / 2)

    let part2 () = 
        Utils.readLines input
        |> List.map (
            Seq.toList 
            >> findMissing
            >> List.map (replaceWithCloser >> toScore) 
            >> getLinePenalty 0UL)
        |> List.filter (fun n -> n > 0UL)
        |> median
