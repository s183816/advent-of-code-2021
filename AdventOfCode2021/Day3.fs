namespace AdventOfCode2021

open AdventOfCode2021.Internals

// Important insight: I should have used transpose!

module AOCDay3 =
    let part1 () =

        // For some reason, (int) '0' = 48
        // Code from https://stackoverflow.com/a/42820286
        let charToInt c = int c - int '0'

        let s =
            Utils.readLinesSeq "input3.txt"
            |> Seq.map Seq.toList
        // |> Seq.map (List.map charToInt)

        let n = Seq.length (Seq.head s)

        seq {
            for i in Seq.init n id do
                let si = Seq.map (Seq.item i) s

                let length0 =
                    Seq.length (Seq.filter (fun x -> x = '0') si)

                if length0 <= Seq.length si / 2 then
                    yield ('1', '0')
                else
                    yield ('0', '1')
        }
        |> Seq.toList
        |> List.unzip
        |> fun (g, e) ->
            let g' =
                "0b" + (new string [| for c in g -> c |])

            let e' =
                "0b" + (new string [| for c in e -> c |])

            int g' * int e'

    let part2 () =

        let rec findRating comp bit =
            function
            | [] -> failwith "Error"
            | [ b ] -> (int) ("0b" + (new string [| for c in b -> c |]))
            | bs ->
                let s =
                    seq {
                        for b in bs do
                            yield List.item bit b
                    }

                let length0 =
                    Seq.length (Seq.filter (fun c -> c = '0') s)

                let length1 = Seq.length s - length0

                let c =
                    if length0 = length1 then
                        if comp 0 1 = 0 then '0' else '1'
                    else if comp length0 length1 = length0 then
                        '0'
                    else
                        '1'

                List.filter (fun list -> List.item bit list = c) bs
                |> findRating comp (bit + 1)

        let s =
            Utils.readLinesSeq "input3.txt"
            |> Seq.map Seq.toList
            |> Seq.toList

        let o2 = findRating max 0 s
        let co2 = findRating min 0 s
        o2 * co2
