﻿namespace AdventOfCode2021

open System.IO
open System.Reflection
open System.Text.RegularExpressions

module program =

    let solveDayPart day part =
        let result =
            Assembly.GetExecutingAssembly().GetTypes()
            |> Seq.find (fun t -> t.Name = sprintf $"AOCDay%i{day}")
            |> fun t -> t.GetMethod(sprintf $"part%i{part}")
            |> fun mi -> mi.Invoke(null, [||])

        printfn $"Solution to Day %i{day} part %i{part}: %A{result}"

    let solveDay day =
        for part in [ 1 .. 2 ] do
            solveDayPart day part

    let printAllSolutions () =

        // Find the number of days solved by finding the latest DayXX.fs
        let daysSolved =
            Directory.GetFiles __SOURCE_DIRECTORY__
            |> Seq.toList
            |> List.filter (fun s -> s.Contains "Day")
            |> List.map
                (fun (s: string) ->
                    Regex.Match(s, "Day([1-9][1-9]?).fs").ToString()
                    |> fun s' -> int s'.[3..s'.Length - 4])
            |> List.max

        for day in [ 1 .. daysSolved ] do
            for part in [ 1; 2 ] do
                solveDayPart day part

            printf "\n"

    [<EntryPoint>]
    let main argv =

        match Seq.toList argv with
        | [] -> printAllSolutions ()
        | sDay :: argvs ->
            let day = int sDay

            match argvs with
            | [] -> solveDay day
            | [ sPart ] -> int sPart |> solveDayPart day
            | _ -> failwith "Maximum two arguments (day part)"

        0 // Escape main function.
