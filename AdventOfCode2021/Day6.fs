namespace AdventOfCode2021

open AdventOfCode2021.Internals

open AdventOfCode2021




module AOCDay6 =

    let input: int list =
        Utils.readLines "input6.txt"
        |> List.head // Input is only one line
        |> fun (s: string) -> Seq.toList (s.Split ",") // Split at commas
        |> List.map int

    let test = [ 3; 4; 3; 1; 2 ]


    // Initialize map: Keys = internal timers, values = 0.
    let initMap =
        ([ 0 .. 8 ], List.init 9 (fun _ -> 0UL))
        ||> List.zip
        |> Map.ofList

    let inputToMap input =
        let incrementValue map key =
            Map.add key (Map.find key map + 1UL) map

        (initMap, input) ||> List.fold incrementValue

    let update acc day individuals =
        match day, individuals with
        | 0, i -> acc |> Map.add 0 0UL |> Map.add 6 i |> Map.add 8 i
        | n, i ->
            let prevValue =
                match Map.tryFind n acc with
                | None -> 0UL
                | Some i' -> i'

            let nextValue =
                match Map.tryFind (n - 1) acc with
                | None -> i
                | Some i' -> i' + i

            acc
            |> Map.add n prevValue
            |> Map.add (n - 1) nextValue



    // Here, days count down in stead
    let rec simulate days pop =
        match days with
        | 0 -> pop
        | n ->
            (Map.empty, pop)
            ||> Map.fold update
            |> simulate (n - 1)

    let getPopCount map =
        (0UL, map) ||> Map.fold (fun sum _ n -> sum + n)

    let part1 () =
        inputToMap input |> simulate 80 |> getPopCount

    let part2 () =
        inputToMap input |> simulate 256 |> getPopCount
