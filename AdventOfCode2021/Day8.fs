namespace AdventOfCode2021

open AdventOfCode2021.Internals

module AOCDay8 =

    // Each element in the input list is itself a list of sets of chars. The
    // first ten sets [0..9] is the signal patterns, the latter four sets
    // [10..] the output values.
    let input =
        Utils.readLines "input8.txt"
        |> List.map (
            fun s -> Seq.toList (s.Split " ")
            >> List.removeAt 10
            >> List.map Set.ofSeq
        )

    let signalPatterns = List.map (List.take 10) input

    let outputValues =
        input |> List.map (fun list -> list.[10..])


    let part1 () =

        // Only interested in ALL the output values, thus grouping is of no
        // essence.
        let values = outputValues |> List.concat

        // Distinct values are 1, 4, 7 and 8 with lengths 2, 4, 3 and 7
        // respectivelly. (Remember values are represented as sets of chars).
        let isDistinct set =
            (Set.count set, [ 2; 3; 4; 7 ]) ||> List.contains

        // Calculate the result
        (0, values)
        ||> List.fold (fun acc set -> if isDistinct set then acc + 1 else acc)

    let part2 () =

        // This will be very brute-forced, but I don't care.

        let ofCount count set = Set.count set = count

        let rec getSum sum sps vs =
            match sps, vs with
            | [], [] -> sum
            | sp :: sprest, v :: vrest ->

                // Get the trivial numbers (unique counts)
                let one = List.find (ofCount 2) sp
                let four = List.find (ofCount 4) sp
                let seven = List.find (ofCount 3) sp
                let eight = List.find (ofCount 7) sp

                // Get numbers of count 6 (0, 6, 9)
                let six =
                    sp
                    |> List.find (fun six' -> ofCount 6 six' && Set.intersect one six' <> one)

                let nine =
                    sp
                    |> List.find (fun nine' -> ofCount 6 nine' && Set.intersect four nine' = four)

                let zero =
                    sp
                    |> List.find (fun zero' -> ofCount 6 zero' && zero' <> six && zero' <> nine)

                // Get numbers of count 5 (2, 3, 5)
                let three =
                    sp
                    |> List.find (fun three' -> ofCount 5 three' && Set.intersect one three' = one)

                let five =
                    sp
                    |> List.find
                        (fun five' ->
                            ofCount 5 five'
                            && Set.intersect five' nine = five'
                            && five' <> three)

                let two =
                    sp
                    |> List.find (fun two' -> ofCount 5 two' && two' <> three && two' <> five)

                // Dictionary from set of chars to a number
                let dict =
                    Map.ofList [ (zero, 0)
                                 (one, 1)
                                 (two, 2)
                                 (three, 3)
                                 (four, 4)
                                 (five, 5)
                                 (six, 6)
                                 (seven, 7)
                                 (eight, 8)
                                 (nine, 9) ]

                // Calculate the output number
                let sum' =
                    1000 * Map.find (List.item 0 v) dict
                    + 100 * Map.find (List.item 1 v) dict
                    + 10 * Map.find (List.item 2 v) dict
                    + Map.find (List.item 3 v) dict

                getSum (sum + sum') sprest vrest

            | _ -> failwith Utils.errMsg

        getSum 0 signalPatterns outputValues
