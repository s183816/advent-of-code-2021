# F# Advent of Code

[Advent of Code](https://adventofcode.com) (AOC) solved using F#. 

This is merely a hobby-project; the code is not the fastest nor prettiest but 
it does solve the problems. 

Please email me at `s183816@student.dtu.dk` if you want to share your own 
solution to a given problem problem. 


## Setup 

The code is written in VSCode with the of the Ionide extension. 

When inside the folder `AdventOfCode2021`, the code can be run: 

- With 0 arguments, the program prints all the solutions available. 

		$ dotnet run 

		Solution to Day 1 part 1: 1343
		Solution to Day 1 part 2: 1378

		Solution to Day 2 part 1: 1990000
		Solution to Day 2 part 2: 1975421260

		Solution to Day 3 part 1: 4118544
		...

- With 1 argument, the programs finds the solution for the given day, that is 
  `dotnet run 4` to get the solutions from day 4: 


		$ dotnet run 4

		Solution to Day 4 part 1: 67716
		Solution to Day 4 part 2: 1830


- With two arguments, the program will calculate and print the solution to 
  for a given day and part: 
  
		$ dotnet run 8 1

		Solution to Day 8 part 1: 548


### One file for each day 

Each day `XX` has its own file named `DayXX.fs` with the general setup:

```
namespace AdventOfCode2021

open AdventOfCode2021.Internals
...

module AOCDayXX =

    let part1 () =
        ...


    let part2 () =
        ...

```
	
Notice, in `AdventOfCode2021.fsproj`, `Utils.fs` must always be the at the top. 
